//DOM это объектная модель документа в которой содержатся все элементы страницы в виде объектов, с помощью DOM
//можно добавлять их, или изменять содержимое.
createList = (arr, parent = document.body) => {
    arr.map(function (currentValue){
        let nestedList = document.createElement('ul')
        let listPoint = document.createElement('li')
        if (Array.isArray(currentValue)){
            parent.append(nestedList)
            createList(currentValue, nestedList)
        } else {
            listPoint.innerText = currentValue;
            parent.append(listPoint)
        }
    })
    let remove = document.getElementsByTagName("li")
    setTimeout (() => document.body.remove(remove), 3000)
}
createList(["hello", "world", ["Kiev", "Kharkiv"], "Odessa", "Lviv"])

